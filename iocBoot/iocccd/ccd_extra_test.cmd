#!../../bin/linux-x86_64/ccd

## You may have to change ccd to something else
## everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/ccd.dbd"
ccd_registerRecordDeviceDriver pdbbase

##debug
epicsEnvSet("DEVIP", "127.0.0.1")
epicsEnvSet("DEVPORT", "12345")
epicsEnvSet("IOCBL", "LPQ")
epicsEnvSet("IOCDEV", "ccd1")


### Config
# Prefix for all records
#epicsEnvSet("PREFIX", "LPQ:ccd1:")
epicsEnvSet("PREFIX", "$(IOCBL):$(IOCDEV):")
# The port name for the detector
epicsEnvSet("PORT",   "GE")
# The queue size for all plugins
epicsEnvSet("QSIZE",  "20")
# The maximum image width; used to set the maximum size for this driver and for row profiles in the NDPluginStats plugin
epicsEnvSet("XSIZE",  "2052")
# The maximum image height; used to set the maximum size for this driver and for column profiles in the NDPluginStats plugin
epicsEnvSet("YSIZE",  "2052")
# The maximum number of time series points in the NDPluginStats plugin
epicsEnvSet("NCHANS", "2048")
# The maximum number of frames buffered in the NDPluginCircularBuff plugin
epicsEnvSet("CBUFFS", "20")
# The maximum number of threads for plugins which can run in multiple threads
epicsEnvSet("MAX_THREADS", "8")
# The search path for database files
epicsEnvSet("EPICS_DB_INCLUDE_PATH", "$(ADCORE)/db")

## 2052x2052 = 4210704 x4 = 16842816
## 2052x2052 = 4210704 x8 = 33685632
epicsEnvSet("NELEMENTS", "4210704")
epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES", "34000000")

# Configuration example for greateyesCameraServer (Ethernet TCP/IP)
#int greatEyesCCDConfig(const char *portName, int maxBuffers, size_t maxMemory, int peltierType,
#                           int interfaceType, const char* IPAddress, int priority, int stackSize)
#greatEyesCCDConfig("$(PORT)", -1, -1, 42223, 3, "$(DEVIP)", 0, 0)
#epicsThreadSleep 3

#drvAsynIPPortConfigure("$(PORT)", "127.0.0.1:50123", 0, 0, 0)
#drvAsynIPPortConfigure($(PORT), "$(DEVIP):$(DEVPORT)", 0, 0, 0)

## Load greateyes records
#dbLoadRecords("$(ADGEYES)/db/greatEyesCCD.template","P=$(PREFIX),R=cam1:,PORT=$(PORT),ADDR=0,TIMEOUT=2,RATE_SMOOTH=0.9")

## Load record instances
#dbLoadRecords("${EXSUB}/db/exsub.db","P=$(PREFIX),R=exsub")
dbLoadTemplate("${TOP}/iocBoot/${IOC}/ccd_extra.substitutions", "BL=$(IOCBL),DEV=$(IOCDEV)")

cd "${TOP}/iocBoot/${IOC}"

#set_savefile_path("/EPICS/autosave")
#set_requestfile_path("${TOP}/iocBoot/${IOC}/reqs")
#set_requestfile_path("$(ADCORE)/ADApp/Db")
#set_pass0_restoreFile("auto_settings.sav")
#set_pass1_restoreFile("auto_settings.sav")

iocInit

#create_monitor_set("auto_settings.req", 30, "P=$(PREFIX)")

## Start any sequence programs
#seq sncxxx,"user=epics"

