#!../../bin/linux-x86_64/ccd

## You may have to change ccd to something else
## everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/ccd.dbd"
ccd_registerRecordDeviceDriver pdbbase

##debug
#epicsEnvSet("DEVIP", "192.168.1.234")
#epicsEnvSet("DEVIP", "127.0.0.1")
#epicsEnvSet("DEVPORT", "12345")
epicsEnvSet("IOCBL", "LPQ")
epicsEnvSet("IOCDEV", "ccd1")


### Config
# Prefix for all records
#epicsEnvSet("PREFIX", "LPQ:ccd1:")
epicsEnvSet("PREFIX", "$(IOCBL):$(IOCDEV):")
# The port name for the detector
epicsEnvSet("PORT",   "GE")
# The queue size for all plugins
epicsEnvSet("QSIZE",  "20")
# The maximum image width; used to set the maximum size for this driver and for row profiles in the NDPluginStats plugin
epicsEnvSet("XSIZE",  "2052")
# The maximum image height; used to set the maximum size for this driver and for column profiles in the NDPluginStats plugin
epicsEnvSet("YSIZE",  "2052")
# The maximum number of time series points in the NDPluginStats plugin
epicsEnvSet("NCHANS", "2048")
# The maximum number of frames buffered in the NDPluginCircularBuff plugin
epicsEnvSet("CBUFFS", "20")
# The maximum number of threads for plugins which can run in multiple threads
epicsEnvSet("MAX_THREADS", "8")
# The search path for database files
epicsEnvSet("EPICS_DB_INCLUDE_PATH", "$(ADCORE)/db")

## 2052x2052 = 4210704 x8 = 33685632
epicsEnvSet("NELEMENTS", "4210704")
epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES", "33700000")

# Configuration example for greateyesCameraServer (Ethernet TCP/IP)
#int greatEyesCCDConfig(const char *portName, int maxBuffers, size_t maxMemory, int peltierType,
#                           int interfaceType, const char* IPAddress, int priority, int stackSize)
#greatEyesCCDConfig("$(PORT)", -1, -1, 42223, 3, "$(DEVIP)", 0, 0)
epicsThreadSleep 3

drvAsynIPPortConfigure("$(PORT)", "127.0.0.1:12345", 0, 0, 0)
#drvAsynIPPortConfigure($(PORT), "$(DEVIP):$(DEVPORT)", 0, 0, 0)

## Load greateyes records
dbLoadRecords("$(ADGEYES)/db/greatEyesCCD.template","P=$(PREFIX),R=cam1:,PORT=$(PORT),ADDR=0,TIMEOUT=2,RATE_SMOOTH=0.9")

# Create a standard arrays plugin, set it to get data from detector driver.
NDStdArraysConfigure("Image1", 5, 0, "$(PORT)", 0, 0)
NDStdArraysConfigure("Image2", 5, 0, "$(PORT)", 0, 0)
NDStdArraysConfigure("Image3", 5, 0, "$(PORT)", 0, 0)
NDStdArraysConfigure("Image4", 5, 0, "$(PORT)", 0, 0)
NDStdArraysConfigure("Image5", 5, 0, "$(PORT)", 0, 0)
NDStdArraysConfigure("Image6", 5, 0, "$(PORT)", 0, 0)
NDStdArraysConfigure("Image7", 5, 0, "$(PORT)", 0, 0)
NDStdArraysConfigure("Image8", 5, 0, "$(PORT)", 0, 0)

# 32-bit images
#dbLoadRecords("NDStdArrays.template", "P=$(PREFIX),R=image1:,PORT=Image1,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),TYPE=Int32,FTVL=LONG,NELEMENTS=$(NELEMENTS)")
#dbLoadRecords("NDStdArrays.template", "P=$(PREFIX),R=image2:,PORT=Image2,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),TYPE=Int32,FTVL=LONG,NELEMENTS=$(NELEMENTS)")
#dbLoadRecords("NDStdArrays.template", "P=$(PREFIX),R=image3:,PORT=Image3,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),TYPE=Int32,FTVL=LONG,NELEMENTS=$(NELEMENTS)")
#dbLoadRecords("NDStdArrays.template", "P=$(PREFIX),R=image4:,PORT=Image4,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),TYPE=Int32,FTVL=LONG,NELEMENTS=$(NELEMENTS)")
#dbLoadRecords("NDStdArrays.template", "P=$(PREFIX),R=image5:,PORT=Image5,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),TYPE=Int32,FTVL=LONG,NELEMENTS=$(NELEMENTS)")
#dbLoadRecords("NDStdArrays.template", "P=$(PREFIX),R=image6:,PORT=Image6,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),TYPE=Int32,FTVL=LONG,NELEMENTS=$(NELEMENTS)")
#dbLoadRecords("NDStdArrays.template", "P=$(PREFIX),R=image7:,PORT=Image7,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),TYPE=Int32,FTVL=LONG,NELEMENTS=$(NELEMENTS)")
#dbLoadRecords("NDStdArrays.template", "P=$(PREFIX),R=image8:,PORT=Image8,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),TYPE=Int32,FTVL=LONG,NELEMENTS=$(NELEMENTS)")

# DOUBLE type images
dbLoadRecords("NDStdArrays.template", "P=$(PREFIX),R=image1:,PORT=Image1,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),TYPE=Float64,FTVL=DOUBLE,NELEMENTS=$(NELEMENTS)")
dbLoadRecords("NDStdArrays.template", "P=$(PREFIX),R=image2:,PORT=Image2,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),TYPE=Float64,FTVL=DOUBLE,NELEMENTS=$(NELEMENTS)")
dbLoadRecords("NDStdArrays.template", "P=$(PREFIX),R=image3:,PORT=Image3,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),TYPE=Float64,FTVL=DOUBLE,NELEMENTS=$(NELEMENTS)")
dbLoadRecords("NDStdArrays.template", "P=$(PREFIX),R=image4:,PORT=Image4,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),TYPE=Float64,FTVL=DOUBLE,NELEMENTS=$(NELEMENTS)")
dbLoadRecords("NDStdArrays.template", "P=$(PREFIX),R=image5:,PORT=Image5,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),TYPE=Float64,FTVL=DOUBLE,NELEMENTS=$(NELEMENTS)")
dbLoadRecords("NDStdArrays.template", "P=$(PREFIX),R=image6:,PORT=Image6,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),TYPE=Float64,FTVL=DOUBLE,NELEMENTS=$(NELEMENTS)")
dbLoadRecords("NDStdArrays.template", "P=$(PREFIX),R=image7:,PORT=Image7,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),TYPE=Float64,FTVL=DOUBLE,NELEMENTS=$(NELEMENTS)")
dbLoadRecords("NDStdArrays.template", "P=$(PREFIX),R=image8:,PORT=Image8,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),TYPE=Float64,FTVL=DOUBLE,NELEMENTS=$(NELEMENTS)")

## Load record instances
dbLoadRecords("${EXSUB}/db/exsub.db","P=$(PREFIX),R=exsub")

## extra records
dbLoadTemplate("${TOP}/iocBoot/${IOC}/ccd_extra.substitutions", "BL=$(IOCBL),DEV=$(IOCDEV)")
dbLoadRecords("${TOP}/iocBoot/${IOC}/ccd_bin.db", "BL=$(IOCBL),DEV=$(IOCDEV)")
## BG records
#dbLoadTemplate("${TOP}/iocBoot/${IOC}/ccd_bg.substitutions", "BL=$(IOCBL),DEV=$(IOCDEV)")

## change directory
cd "${TOP}/iocBoot/${IOC}"

# Load all other plugins using commonPlugins.cmd
< commonPlugins.cmd

## load extra records
dbLoadRecords("${TOP}/iocBoot/${IOC}/roisup.db", "BL=$(IOCBL),DEV=$(IOCDEV)")
dbLoadRecords("${TOP}/iocBoot/${IOC}/roiabs.db", "BL=$(IOCBL),DEV=$(IOCDEV),N=2")

## autosave
set_savefile_path("/EPICS/autosave")
set_requestfile_path("${TOP}/iocBoot/${IOC}/reqs")
set_requestfile_path("$(ADCORE)/ADApp/Db")
set_pass0_restoreFile("auto_settings.sav")
set_pass1_restoreFile("auto_settings.sav")

## init
iocInit

## autosave
create_monitor_set("auto_settings.req", 30, "P=$(PREFIX)")

## Start any sequence programs
#seq sncxxx,"user=epics"

# Preset data output folder
dbpf $(PREFIX)Proc1:TIFF:FilePath /EPICS/data
dbpf $(PREFIX)Proc2:TIFF:FilePath /EPICS/data
dbpf $(PREFIX)Proc3:TIFF:FilePath /EPICS/data
dbpf $(PREFIX)netCDF1:FilePath /EPICS/data
dbpf $(PREFIX)TIFF1:FilePath /EPICS/data
dbpf $(PREFIX)TIFF2:FilePath /EPICS/data
dbpf $(PREFIX)TIFF3:FilePath /EPICS/data
dbpf $(PREFIX)JPEG1:FilePath /EPICS/data
dbpf $(PREFIX)Nexus1:FilePath /EPICS/data
dbpf $(PREFIX)Nexus1:TemplateFilePath /EPICS/data
dbpf $(PREFIX)Magick1:FilePath /EPICS/data
dbpf $(PREFIX)HDF1:FilePath /EPICS/data

## wait to update BIN values
epicsThreadSleep 3
dbpf $(PREFIX)b:init_lastBinX.PROC 1
dbpf $(PREFIX)b:init_lastBinY.PROC 1
## Enable ROI(BIN) transformer
epicsThreadSleep 1
dbpf $(PREFIX)b:1:roiXcalc.DISA 0
dbpf $(PREFIX)b:1:roiYcalc.DISA 0
dbpf $(PREFIX)b:2:roiXcalc.DISA 0
dbpf $(PREFIX)b:2:roiYcalc.DISA 0
dbpf $(PREFIX)b:3:roiXcalc.DISA 0
dbpf $(PREFIX)b:3:roiYcalc.DISA 0
